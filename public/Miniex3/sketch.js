 {

   var maxDiameter;
   var theta;
  function setup() {
    createCanvas(600,500);
  maxDiameter = 30;
 theta = 0;

}

function draw() {

  background(180, 234, 245, 85);
    //pedals//
    push();

 var diam = 170 + sin(theta) * maxDiameter ;

strokeWeight(30);
stroke(245, 219, 106);
fill(245, 219, 106);
//top pedal//
ellipse(300,150,50,diam);
rotate(HALF_PI/2.4);
//upper right pedal//
ellipse(400,-80,50,diam);

//lower left pedal//
strokeWeight(40);
ellipse(370,75,50,diam);
rotate(HALF_PI/1.12);
ellipse(70,-290,50,diam);
//lower right pedal//
strokeWeight(40);
rotate(TWO_PI/25);
ellipse(-30,-450,50,diam);
 theta += .06; //speed of pulsing//
 pop();
 //center//
 noStroke();
 fill(120, 66, 18);
 circle(300,228,80);

 if (mouseIsPressed){
   console.log(mouseX,mouseY);
 }

}

}
