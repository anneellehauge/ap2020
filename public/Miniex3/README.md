![screenshot](ex3screenshot.png)

*  [link to repository](https://gitlab.com/anneellehauge/ap2020/-/tree/master/public/Miniex3)
*  [link to my work](https://anneellehauge.gitlab.io/ap2020/Miniex3)



**Describe your throbber design, both conceptually and technically.**
My concept: I wanted to create something that would be calming to look at because the imidiate response to waiting when a throbber accurs is frustration. So i didn't want any fast spinning, bouncing or energetic motion of any kind. 
This is why I made the decision to create a pulsating effect. Still with a determined focus on calmness as the key feeling around my throbber I wrote down some ideas for objects that were associated with joy and a slow pace and i ended up liking the idea of a pulsating sunflower. 
The technical part: I had no idea how to make something pulsate. So I googled "p5js pulsing" and borrowed some syntax from the example that came up. 
I have used rotate with HALF_PI and TWO_PI for the pedals pulsating. I had never used rotate before and tried working with degrees because I understand how to use degrees in general, but when i tried setting angleMode to degreese, my pedals stopped moving altogether. 
This was ultimately it became unfortunate that I borrowed the syntax for the pulsing effect and just played around with that because it resulted in me not understanding how I used rotate. When trying the different uses of PI the motion worked. I still don't know why and I don't fully understand how PI works. 
But again, I just tried changing different parameters and made it work the best I could. For the next mini exercise I'll make sure to build up my syntax while help from instructors is still available so that I understand while coding. I think this is important. 



**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way?**
In the example of pulsing I found there was a parameter called theta. I tried figuring out what this means but I couldn't. I just tried tampering with it and discovered that it relates to the speed of the pulsing. The higher I made the number, the faster my ellipses pulsate. 
I aimed for a tempo which was not too slow or too fast. I wanted to express to the user that something will happen soon and that data is being processed but still not make it the tempo so fast that it was visually disturbing or emotionally stressful. 

**How is time being constructed in computation?**
If you were to create a throbber which resambled more accurately how time works in computation you might create something unpredictible, tangled and working with incredible speed. But this is not what I believe a user wants to see while wating. Perhaps it is unfortunate in some ways that most throbbers signify an illusion of flow and stream, something that is steady and "on its way" in a controlled fashion. 
But I do still believe that a user needs something soothing to look at while waiting because the ordinary user has enough going on while looking at screens for them to consider yet another thing. Perhaps it should be something you could costumize yourself. Maybe users should have more options built into their devices regarding throbbers. 

