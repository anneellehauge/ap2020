![screenshot](miniex5.png)
* [TRY MY PROGRAM](https://anneellehauge.gitlab.io/ap2020/Miniex5)
*  [My code](https://gitlab.com/anneellehauge/ap2020/-/blob/master/public/Miniex5/sketch.js)



**What is the concept of your work?**
For this exercise I chose to revisit my first project and change different aspects of it. 
The first project was just about creating something with changing the color of the background as a minimum. 
But in all the following exercises a lot more thought and reflection went into creating a concept, a red thread connecting the syllabus of the course and my advancing skills in programming. 
The idea behind going back to the very first exercise was to apply syntax and concepts from the following exercises to underline what I have learned both conceptually and technically so far, and to make the viewer perceive my first project differently. 

**What have you changed and why?**
The first thing I wanted to change in my miniex 1 was the fact that there was no interactivity. It is just a static picture. I have added two kinds of interactivity triggered by pressing the mouse: stars in the background appearing gradually and the ability to change the color of the alien by clicking the mouse or pressing it. 
In my miniex 4 I made small buttons appear randomly on the canvas when pressing the mouse. This I adopted to my miniex 1 to create interactivity and also to shorten my line of code because in making my first program I just repeated the 'circle' syntax and applied all the positions manually.
I also changed the framerate from the standard of 60/sec to 7/sec mainly to slow down the changing of colors on the alien when you press the mouse, but I also quite liked the effect on the stars appearing at a slower pace because it created the feeling of waiting, perhaps even frustration connected to seeing a throbber in action and since it was my intention to bring in elements from all my previous projects to this one, I was quite happy with this as it mimics some points made in miniex 3.
This idea of implementing things from all the previous exercises was what led to the decision of the user being able to change the appearance of the alien. 
This is supposed to be an element from miniex2 where we were asked to create emojis. Here the main issue was about representation and I thought it was interesting to make the non-human blob which is my alien into an emoji and also to provide the program with more color and more advanced syntax than what i used in the first program. 


**Reflection upon Aesthetic Programming**
The way I have understood the main point of Aesthetic Programming is that it is a method of looking at and dealing with code where the emphasis is on questioning different aspects of programming, reflecting upon your own work with code and have a critical approach to digital culture. 
This exercise has made me reflect on the main points I remembered from previous texts read and previous concepts I made based on these. I feel that this is in itself in the spirit of Aesthetic Programming as it has been a process of asking questions and thinking about the issues we have been dealing with until this point of the course. 


