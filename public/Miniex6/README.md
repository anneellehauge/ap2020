![screenshot](minix6.png)
*  [RUNME](https://anneellehauge.gitlab.io/ap2020/Miniex6)
*  [Sourcecode](https://gitlab.com/anneellehauge/ap2020/-/blob/master/public/Miniex6/sketch.js)

**Game/game objects: the idea**
The reason for me to simply call it an 'idea' is because due to the current Corona situation me not being able to meet with peers or instructors has lead to a not so succesful project this time. But the plan was initially that I wanted to create a game centered around the story of Noahs Ark and focus the idea of objects on making different animals falling from the sky. The boat i have made is meant to represent the ark. You would have to help Noah save as many animals as possible from drowning using the arrow keys to move the boat from left to right. I managed to get the boat moving and the "animals" to fall from random x-locations in the down falling motion I wanted. I am also happy that i managed to create a class and use it, but I did not succed in using the class to give my objects diiferent characteristis which supposably is actually one of the main benefits of making classes.   


