[RUNME](https://anneellehauge.gitlab.io/ap2020/Miniex6)
![screenshot](miniex7.png)
Disclaimer: For this mini exercise you will have to excuse my program as it is not nearly done, but I have had to prioritize other things this week. I did manage to think about the reading and come up with an idea so I am going to tell rather than show this week. 

**My idea** 

From reading the text Randomness by Nick Montfort I gathered that games and the randomness/chance/luck aspect of games is very important to people. 
It is the very thing that drives them to even play. 
Montfort tells it this way: "Once the outcome of a game is known, the game becomes meaningless. Incorporating chance into the game helps delay the moment when the outcome will become obvious." (Montfort, 2012:122)
This is what made me want to create the game of noughts and crosses. 
What I intented to happen was for the user of the program to put a cross down at one of the turfs in the grid which would cause the program to generate a nought at one of the other remaining turfs randomly, so that the outcome of the game would be random each time. 
I also had an idea to make the aesthetics in the theme of chance and randomness. The grid was meant to change colors randomly like the background but in a different range. I wanted to symbolise another point from the text Randomness which is that the chance is deliberately incorporated in a strategic way and is really not that much of a chance at all but more of a spectrum and one of the ways in which i wanted to show this was to make the color ranges different but still chosen randomly. 

**Rules in my program and what was supposed to happen in it over time**
The rules in my program would have been those of a regular game of noughts and crosses with the adjustment of the opponent being a program autogenerating a move or "respons". 

**Feedback: can you help me?** 
If you had my idea how would you have done it? What would you change? what kind of syntax would you implement? 