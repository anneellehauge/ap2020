let r,g,b;

function setup() {

  createCanvas(400,400);


}

function draw() {

//background
  frameRate(1);
  r = random(200,255);
  g = random(200,255);
  b = random(200,255);

  background(r,g,b);

//grid
stroke(20);
strokeWeight(3);
line(133.3,20,133.3,380);
line(266.6,20,266.6,380);
line(20,133.3,380,133.3);
line(20,266.6,380,266.6);

}
