let button;
let img;
let showButton = true;


function preload(){
    img = loadImage('miniex4baggrund.png');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
}

function draw() {
  background(255);
  image (img,0,0,windowWidth,windowHeight);

  //allow button//
  if (showButton) {

    strokeWeight(5);
    stroke(40, 180, 99);
    fill(88, 214, 141);
    ellipse(windowWidth/2, windowHeight/2, 200, 200);
    textSize(32);
    fill(20);
    noStroke();
    textStyle(BOLD);
    textFont("Fjalla One");
    text('ALLOW',580, 370);
    textSize(3.5);
    text('that is if you do not mind giving up your privacy to Facebook when you go online',580,373);


  }

  //data collecting signs//
  if (mouseIsPressed) {
    button = createButton('collecting data');
    button.position (random(10,1200),random(20,700));
    showButton = false;



    console.log(mouseX,mouseY);
  }

}
