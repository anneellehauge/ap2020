*  [RUN THE PROGRAM](https://anneellehauge.gitlab.io/ap2020/miniex4)

![screenshot](miniex4screenshot.png)

**Title and description**

Visible Break-in -
This project is an attempt to criticize how we accept and allow data collecting as a part of our everyday lives.
Facebook is a massive sinner when it comes to capturing and exploiting our data to their own economic advantage.
This is why my project comments on the data capturing on this particular platform.
I chose the name Visible Break-in for my program because this is the general idea and comment I am trying to get across to the audience.
In my program I try to visualize the quick and often thoughtless action of accepting terms via a button online.
The button is used to trigger the “break-in” which is Facebook collecting data everywhere uncontrollably.
The point is that there is no point of return when you choose to press the button.

**The program - what have I used and learnt?**

The first thing I experimented with was making a button.
As advised in the instructor class Wednesday I went to the W3Schools.com site and looked at the guidance there for creating and styling buttons.
I originally had an entirely different idea and wanted multiple random buttons to show up and ask the user different random questions.
I played around with the keyword ‘button.position’ and ‘random’ in the function ‘mousePressed’ and just temporarily put “collecting data” as the caption, but I ended up liking the effect of multiple small and anonymous looking buttons appearing randomly on the screen when I pressed the mouse.
This is how I got the idea of creating this effect of uncontrollable data collecting on a platform everyone uses.
To do this I needed to import an image to the canvas of my program which I had not tried before.
Because I wanted the image to be a screenshot of a browser to create the illusion that the user was in fact using that browser, I thought I could just make the image my canvas which later proved to be difficult because the draw-function does not work if there is no regular canvas to draw on.
This had the unfortunate effect that the program created a white canvas to draw my blue ellipse on which was underneath the image so you had to scroll down to see the ellipse that was supposed to be a different button appearing on top of the screenshot.

**Further comments on what I have learned after making my program:**

My program turned out to be symbolic which in hindsight may not have been the intention with this exercise.
I only experimented with user inputs from the mouse and had I understood before reflecting in my readme that this was about exploring multiple forms of interactivity, inputs and outputs I would have thought about the nature of my program in a very different way.
I believe I have focused a bit too much on just making a few things work in my program rather than experimenting with several things which was actually a part of the RUNME brief… I’ll be sure to be more careful in reading the intention of the exercise before making my next program.

**How my program and thinking addresses the theme 'capture all'**

As mentioned above I think the best way of addressing the theme ‘capture all’ would have been to actively try to capture ALL kinds of data when running my program using for example the microphone, the camera and so on.
Instead I ended up addressing the theme in a quite simple way focusing on choice of words, text size, colors and just the general visual impression.
That said I am happy with my result. Especially the data collecting buttons appearing infinitely through interactivity on the “page” because this emphasizes the ongoing capturing of all kinds of data when using Facebook.