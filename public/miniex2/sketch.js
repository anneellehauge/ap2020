function setup() {
  // put setup code here
  createCanvas(800,600);
  background(300);
}

function draw() {
//the modern muslim woman//
//hijab behind head//
noStroke();
fill(255,220,122);
ellipse(295,195,120,145);
rect(242,230,110,45,15);
rect(232,248,130,50,15);
//head//
noStroke();
fill(255,200,160);
ellipse(295,196,93,110);
//left eye//
strokeWeight(2)
stroke(20)
fill(255);
bezier(260,188,275,167,285,187,287,190);
strokeWeight(1)
stroke(0,168,215);
fill(20)
circle(273,185,10);
//right eye//
strokeWeight(2)
stroke(20)
fill(255);
bezier(330,189,318,168,304,187,303,190);
strokeWeight(1)
stroke(196,120,7);
fill(20)
circle(316,186,10);
//upperlip left side//
noStroke();
fill(220,0,0);
bezier(300,225,290,218,287,216,273,225);
//upperlip right side//
bezier(290,225,295,227,300,210,318,225);
//buttom lip//
bezier(273,225,280,233,300,240,318,225);
//hijab front//
fill(255,220,122)
noStroke();
arc(295,170,93,70,radians(180),radians(0));

//man crying//
//left ear//
fill(255,200,160);
ellipse(453,185,20,25);
//right ear//
ellipse(547,185,20,25);
//head//
noStroke();
fill(255,200,160);
ellipse(500,196,93,110);
//inner left ear//
fill(250,163,128);
arc(455,185,15,15,radians(100),radians(280));
//inner right ear//
fill(250,163,128);
arc(545,185,15,15,radians(260),radians(85));
//beard//
fill(144,98,6);
noStroke();
arc(500,203,94,98,radians(0),radians(180));
fill(255,200,160);
rect(478,215,45,18,7);
//mouth//
fill(250,163,128);
rect(488,223,25,2,10);

//left eye//
fill(255);
ellipse(480,180,22);
stroke(17,127,191);
strokeWeight(2);
fill(20);
circle(480,186,9);
//lid,left//
noStroke();
fill(255,200,160);
arc(480,180,22,22,radians(180),radians(0));
//right eye//
fill(255);
ellipse(520,180,22);
stroke(17,127,191);
strokeWeight(2);
fill(20);
circle(520,186,9);
//lid,right//
noStroke();
fill(255,200,160);
arc(520,180,22,22,radians(180),radians(0));
//tear//
fill(128,216,243);
triangle(530, 187, 533, 193, 527, 193);
arc(530,193,6,8,radians(0),radians(180));
//beanie//
fill(249,176,68);
rect(453,152,95,20,9);
arc(500,153,75,50,radians(180),radians(0));

 // put drawing code here
}
