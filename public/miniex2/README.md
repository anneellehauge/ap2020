![screenshot](minix2screenshot.png)
*  [link to repository](https://gitlab.com/anneellehauge/ap2020/-/tree/master/public/miniex2?fbclid=IwAR1e2XlZlPHF6Z3nmLabK6TLugMUCfjcnS01Dk_yjeVlL8d7K7aPLKr-fZY)
*  [link to my work](https://anneellehauge.gitlab.io/ap2020/miniex2)


*  For this exercise I have experimented with a lot of different shapes in different sizes. 
I tried using rectangles with soft edges, triangles and arcs for the first time.
Also I discovered a browser which shows the arguments for any color in RGB which made way for some more interesting and aestheticly pleasing results. 


*  The first emoji I made was the one to the left which I call "the modern muslim woman". This is an attempt to represent the women who wear hijab but also want to express themselves through makeup.
There is a lot of predujices in society concerning women who wear hijab and a lot of muslim women (especially young women) use social media to express their right and wish to wear makeup and colorful hijabs. 
I deliberately made each of her eyes a different color because I didn't want her to have a specific ethnicity. I wanted to express respect for the personal choice of religion and image. 


*  The second emoji represents a masculine man crying. I wanted to challenge the normative assumption that "real men don't cry". 
We all cry and this I feel should be represented and acknowledged in pop-culture everywhere. An emoji is a good place to start. 



