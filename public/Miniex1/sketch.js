function setup() {
  // put setup code here
  createCanvas(700,500);
  background(0,0,20);
}

function draw() {
//stars//
noStroke()
circle(100,400,5);
circle(40,460,5);
circle(250,440,5);
circle(680,400,5);
circle(690,90,5);
circle(620,35,5);
circle(400,20,5);
circle(370,40,5);
circle(345,150,5);
circle(135,70,5);
circle(200,100,5);
circle(265,10,5);
circle(60,25,5);
circle(30,230,5);
circle(430,450,5);
circle(470,340,5);
circle(380,500,5);
circle(660,300,5);
circle(680,200,5);
circle(300,270,5);
circle(550,400,5);
circle(100,270,5);
circle(600,480,5);
circle(350,380,5);
circle(300,340,5);

  //alien//
  //body//
    strokeWeight(1);
  noStroke();
  fill(0,220,0);
  ellipse(200,300,50,100);
  //left arm//

  //head//
  noStroke()
  fill(0,220,0);
circle(200,240,60);
//eyes//
fill(250);
//left eye//
circle(180,235,30);
fill(0,0,20);
circle (180,235,10)
//right eye//
fill(250)
circle(220,235,30)
fill(0,0,20);
circle(220,235,10)

//planet//
strokeWeight(1)
fill(220,0,250);
stroke(20);
circle(480,160,250);
//planet ring
noFill();
strokeWeight(20)
stroke(255,0,100);
bezier(475, 200, 65, 155, 350, 90, 365, 93);
bezier(475, 200, 750, 210, 759, 105, 600, 100);
 // put drawing code here
}
